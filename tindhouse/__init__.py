from flask import Flask
from flask import request
from flask import jsonify
from flask import render_template

import requests
import json
import os
import urllib2
from pymongo import MongoClient
from bson.son import SON

app = Flask(__name__)

mongohost = 'localhost'
port = '27017'
dbname = 'tindhouse'

muri = "mongodb://" + mongohost + ":" + port
mconn = MongoClient(muri)
db = mconn[dbname]

zillowHomes = "http://www.zillow.com/homes/"

def getCityFromIp(ipnum):
    #req = requests.get('http://api.hostip.info/get_json.php?ip='+ipnum)
    req = requests.get('http://freegeoip.net/json/'+ipnum)
    resp = req.json()

    print resp

    city = resp.get("city")
    state = resp.get("region_code")
    city = city + ", " + state
    return city

@app.route("/")
def story():
    # Render the a txt box & list of common local websites 

    return "<html><br><p><h1 align='center'>fuck off</h1></p></html>"
    #return app.root_path

@app.route("/listings", methods=["POST"])
def getListings():
    # Get the json zip code from the request
    zipJ = request.get_json()
    zipCode = zipJ["zipCode"]
    bed = "3"
    bath = "2"
    minprice = "100000"
    maxprice = "500000"
    page = "1"


    zURL = zillowHomes + zipCode + '_rb/'

    #Get the geo rect from the zillow
    resp = requests.get(zURL)
    cook = urllib2.unquote(resp.cookies['search']).decode('utf8')
    rect = cook.replace("%2C",",")
    indxS = rect.find("rect=")
    indxE = indxS + 49
    rect = rect[indxS+5:indxE].rstrip()

    #Now send the get listing get to Zillow
    searchURL = "http://www.zillow.com/search/GetResults.htm?spt=homes&status=100000&lt=111100&ht=111101&pr="+minprice+","+maxprice+"&bd="+bed+"&ba="+bath+"&days=any&ds=all&pmf=0&pf=0&rect=-"+rect+"&p="+page+"&sort=days&search=maplist&disp=1&rid=60998&isMapSearch=true&rt=7&listright=true&responsivemode=defaultList&isMapSearch=true"

    re = requests.get(searchURL)

    print searchURL

    print re.text
    

    return "hello"

@app.route("/news", methods=["GET"])
def news():
    addr = request.remote_addr
    topic = ''

    # Request the news storries from nbc news
    urls = []
    req = requests.get('http://data.nbcnews.com/drone/api/query/nbcnews/webapp/1.0/entriesbytopic?topic=tech')
    resp = req.json()
    jArray = []
    jsonElements = dict()
    for v in resp.get("results"):
        jsonElements.update({"headline":v.get("headline")})

        #get the html in body
        for c in v.get('body'):
            if (c.get('type')=='p' and c.get('html') != ''):
                jsonElements.update({'html':c.get('html')})

    print str(jsonElements)

    # jsonElements.update({'city':getCityFromIp(addr)})
    jsonElements.update({'city':getCityFromIp("207.245.67.3")}) 

    jArray.append(jsonElements)

    return str(jArray)

@app.route("/score", methods=["POST"])
def score():

    # Get url
    url = request.form['surl']

     # Get score
    scoreVal = request.form['score']

    arrScore = []

    # Assign Score Values to an array
    if (scoreVal == "like"):
        # The Like value selected
        arrScore = [1,0,0]

        # The meh value selected
    elif (scoreVal == "meh"):
        arrScore = [0,1,0]

        # The hateit value selected
    elif (scoreVal == "hateit"):
        arrScore = [0,0,1]

    likeit = arrScore[0]
    meh = arrScore[1]
    hatedit = arrScore[2]

    # city = getCityFromIp(request.remote_addr)
    # city = getCityFromIp("207.245.67.3")
    city = getCityFromIp("50.182.96.251")

    val = {"url":url, "city":city, "likeit":likeit, "meh":meh, "hatedit":hatedit}

    db.news.insert(val)

    # Get values to add to the graph data
    cities =[]
    likes = []
    meh = []
    hatedit = []
    
    #get cities
    # cit = db.news.aggregate({"$group":{"_id":{"url":"$url","city":"$city"}}})
    cit = db.news.aggregate({"$group":{"_id":{"url":"$url","city":"$city"}, "likeit":{"$sum":"$likeit"},"meh":{"$sum":"$meh"},"hatedit":{"$sum" :"$hatedit"}}});

    # print "This is: "+ str(cit)
    print cit.get("result")

    for c in cit.get("result"):
        cities.append(c["_id"].get("city")) #get city values
        likes.append(c["likeit"]) # Get likeit score
        meh.append(c["meh"])    # Get meh score
        hatedit.append(c["hatedit"])    # Get hatedit score

    # Convert the unicode list to string
    cities = [str(c) for c in cities]

    return render_template('editorview.html', url=url, cities=cities, likes=likes, meh=meh, hatedit=hatedit)

if __name__ == "__main__":
    app.run(debug="True")