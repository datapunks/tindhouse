#!/usr/bin/python
import sys
import logging
import os

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/opt/tindhouse/")

from tindhouse import app as application
application.secret_key = os.urandom(24)
